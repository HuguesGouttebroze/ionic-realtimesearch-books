import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'my ionic app & angular back',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
